Telegram Bot
============

Este script implementa um bot multifuncional para o Telegram sem usar nenhuma lib de abstração fora os módulos nativos do Node.js.

Para executar esse script é preciso antes cadastrar um Bot no Telegram.
https://core.telegram.org/bots#create-a-new-bot
E então definir algumas variáveis de ambiente para configurar o bot.

Para usuar você deve ter o `coffee` instalado globalmente, instale as dependencias com `npm install`

Saiba mais sobre como inicializar com as informações que ele imprime ao ser inicializado sem configuração do ambiente:
```
$ npm start
```

Para testar use o `npm test`, você pode testar modulos específicos passando o nome como parametro:
```
npm test memo util
```
