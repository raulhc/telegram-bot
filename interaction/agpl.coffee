# Show me the code, to respect the AGPL.

{randFromName} = require '../lib/util'

exports.showMeTheCodeRE = /^\s*show(\s+me)\s+(the|your)\s+(code|source)[\.!?\s]*$/i

exports.listen = (msgCtx)->

  if msgCtx.text.match exports.showMeTheCodeRE
    msgCtx.answer "Aqui #{randFromName msgCtx.from}: #{msgCtx.bot.srcRepo}"

