# Answer Date/Time requests

{dig2} = require '../lib/util'

oneDay = 1000 * 60 * 60 * 24

ending = '\\s\\.:,;!?'
endingG = "[#{ending}]*"

months = 'jan fev mar abr mai jun jul ago set out nov dez'.split ' '
monthsRE = (///^#{m}///i for m in months)
monthsList = "(?:#{months.join '|'})[a-zç]*"

exports.askForHour = ///(^|\s)que\s+horas?\s+s[aã]o(\s+agora)?#{endingG}$///i
exports.askForDay = ///(?:^|\s)que\s+(?:dia|data)\s+(?:e|é|eh|foi|ser[aá])\s+(ontem|hoje|amanh[aã])?#{endingG}$///i

displayWords = '(?:mostre|mostrar|escreva|escrever|apresente|apresentar|fa[cç]a|fazer|desenhe|desenhar)'
askForCalendar = "#{displayWords}(?:\\s+o)?\\s+calend[aá]rio"
thisMonth = '(?:des[st]e|para es[st]e)\\s+m[eê]s'
exports.calendarThisMonthRE = ///(?:^|\s)
  #{askForCalendar}(\s+#{thisMonth})?
  (?:#{endingG}|$)|
  ^\s*calend[aá]rio(\s+#{thisMonth})?#{endingG}\s*$///i

prevMonth = 'm[eê]s\\s+(?:passado|anterior)'
nextMonth = 'pr[oó]ximo\\s+m[eê]s|m[eê]s\\s+que\\s+vem'
specMonth = "(?:m[eê]s\\s+)?(?:de\\s+)?(?:#{monthsList}|[0-9]+)"
prevMonthRE = ///#{prevMonth}///i
nextMonthRE = ///#{nextMonth}///i
specMonthRE = ///#{specMonth}///i

askForSomeMonth = "#{nextMonth}|#{prevMonth}|#{specMonth}"
#askForMonthAndYear = "\\s+(?:d[oe]\\s+)?(#{askForSomeMonth})(?:\\s+(?:de\\s+)?([0-9]{2,4}))?"
#askForMonthAndYear = "\\s+(?:d[oe]\\s+)?(#{askForSomeMonth})"

exports.calendarSomeMonthRE = ///^\s*(?:(?:.*\s)?#{displayWords}\s+)?(?:o\s+)?
  calend[aá]rio\s+(?:d[oe]\s+)?(#{askForSomeMonth})(?:\s+de)?(?:\s+([0-9]{2,4}))?#{endingG}///i

prevYear = 'ano passado'
nextYear = 'pr[oó]ximo ano|ano que vem'
thisYear = 'des[st]e ano'
specYear = '(?:ano\\s+(?:de\\s+)?)?[0-9]{2,4}'
prevYearRE = ///#{prevYear}///i
nextYearRE = ///#{nextYear}///i
thisYearRE = ///#{thisYear}///i
specYearRE = ///#{specYear}///i

askForSomeYear = "#{prevYear}|#{nextYear}|#{thisYear}|#{specYear}"

exports.calendarSomeYearRE = ///^\s*(?:(?:.*\s)?#{displayWords}\s+)?(?:o\s+)?
  calend[aá]rio\s+(?:d[eo]\s+|para\s+(?:o\s+)?)?
  (#{askForSomeYear})#{endingG}///i


exports.listen = (msgCtx, callback=(->))->
  today = new Date
  tt = new (require '../lib/time2text') 'Pt-BR'
  try

    # Be a Clock
    if msgCtx.text.match exports.askForHour
      now = new Date
      msgCtx.answer "Pra mim são #{tt.getTime now} (#{dig2 now.getHours()}:#{dig2 now.getMinutes()}).", callback

    # Say the date
    else if match = msgCtx.text.match exports.askForDay
      reactToDateRequest msgCtx, tt, match[1], callback

    #### Calendar Section ####

    # Some Year Calendar
    else if match = msgCtx.text.match exports.calendarSomeYearRE
      reactToYearCalendarRequest msgCtx, tt, match[1], callback

    # This Month Calendar
    else if msgCtx.text.match exports.calendarThisMonthRE
      msgCtx.answerHTML tt.mkCalendar(today.getMonth()), callback

    # Some Month Calendar
    else if match = msgCtx.text.match exports.calendarSomeMonthRE
      reactToMonthCalendarRequest msgCtx, tt, match[1], match[2], callback

    else
      do callback

  catch err
    err.message += '\nInput: ' + msgCtx.text
    msgCtx.bot.admDebug err, callback

reactToDateRequest = (msgCtx, tt, day, callback)->
  day = day.toLowerCase()
  if 'ontem' is day
    date = new Date Date.now() - oneDay
    msgCtx.answer "Ontem foi #{tt.getDate date}.", callback
  else if 'amanh' is day[0..4]
    date = new Date Date.now() + oneDay
    msgCtx.answer "Amanhã será #{tt.getDate date}.", callback
  else
    date = new Date
    msgCtx.answer "Hoje é #{tt.getDate date}.", callback


reactToMonthCalendarRequest = (msgCtx, tt, month, year, callback)->
  today = new Date
  if month.match prevMonthRE
    month = today.getMonth() - 1
  else if month.match nextMonthRE
    month = today.getMonth() + 1
  else if month.match specMonthRE
    month = month.split ' '
    month = month[month.length-1]
    if month.match /[0-9]+/
      month = parseInt(month)-1
    else
      for mRE,i in monthsRE
        if month.match mRE
          month = i; break
    unless 'number' is typeof month and 0 <= month <=11
      throw 'Bad calendar month request.'
  else
    throw 'Bad calendar request match.'
  year = parseInt(year) or today.getFullYear()
  year += 1900 if year < 100
  msgCtx.answerHTML tt.mkCalendar(month, year), callback


reactToYearCalendarRequest = (msgCtx, tt, year, callback)->
  if year.toString().match prevYearRE
    year = (new Date).getFullYear() - 1
  else if year.toString().match nextYearRE
    year = (new Date).getFullYear() + 1
  else if year.toString().match thisYearRE
    year = (new Date).getFullYear()
  else if year.toString().match specYearRE
    year = parseInt year
    year += 1900 if year < 100
  else
    throw 'Bad calendar year request.'
  cal = ( tt.mkCalendar m, year for m in [0..11] ).join '\n \n'
  msgCtx.answerHTML cal, callback

