# Interact with Hello messages

{rand, randFromName} = require '../lib/util'

exports.helloRE = helloRE = /^\s*(he+llo+w?|o+i+|h?ol[aá]|hi+)\s*[\.!?]*\s*$/i

exports.listen = (msgCtx)->

  if ( msgCtx.toMe and msgCtx.text.match helloRE ) \
     or msgCtx.srcMessage.text.match helloRE
    msgCtx.chatCtx.sendMsg rand ["Oi #{randFromName msgCtx.from}!", 'Olá!']

  # return lockContext: {
  #   # optional returning command that disable other interaction modules for future messages.
  #   module: <ref to otherModule> # OPTIONAL. Lock the context no for this module, but to `otherModule`.
  #   timeout: 60 # time in minutes to forgot the context.
  # }

