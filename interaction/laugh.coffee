# Interact with laughing messages

{rand} = require '../lib/util'

exports.laughRE = laughRE = /kkkk|huehue|h+a+h+a+|h+e+h+e+|h+i+h+i+|😄|😂|😝|😜|😅/i

# Test the ignore delay.
# That makes this interaction module stateless as it is not using the proc memory.
exports.laughBelowTimeout = laughBelowTimeout = (chatCtx)->
  time = chatCtx.get('laughTimeout') or 0
  if 0 < time < Date.now()
    chatCtx.set 'laughCounter', 0 # reset counter after ignore delay
  return time > Date.now()

exports.listen = (msgCtx)->
  chatCtx = msgCtx.chatCtx
  if msgCtx.text.match laughRE
    return if laughBelowTimeout chatCtx
    chatCtx.inc 'laughCounter'
    if chatCtx.get('laughCounter') > 1
      if chatCtx.get('laughCounter') is 3
        chatCtx.sendMsg rand ['Tá tá...', '😄😊', 'nem teve tanta graça']
      if chatCtx.get('laughCounter') is 5
        chatCtx.sendMsg rand ['como riem!', 'já deu...', 'nem teve tanta graça']
    else # laughCounter is 0
      chatCtx.sendMsg rand ['hehe...', 'hehehehe!', 'hihihi...', '😄😄😅', 'Rá!']
      chatCtx.set 'laughTimeout', Date.now() + 30*60*1000 # 30 minutes

