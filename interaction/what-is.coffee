wikipedia = require '../lib/wikipedia'
ddg = require '../lib/ddg' # DuckDuckGo search
{log:logger, simpleHTML} = require '../lib/util'

exports.queryRE = /(^|\s)(o que|quem) (e|é|eh|significa)\s*(o|a|um|uma)?\s+([^?!.]+)([?!.])?/i

# "O que é..." faz busca na Wikipédia. Se não achar faz no DDG.
exports.listen = (msgCtx)->
  match = msgCtx.text.match exports.queryRE
  if match
    query = match[5]
    q = query.replace /["']/g, ''
    wikipedia.query q, ['pt', 'en'], (err, resp)->
      if err?
        logger.warn "Wikipedia fail for query \"#{query}\" -- tentando com DDG...", err
        exports.findByDDG msgCtx, match, q
      else
        txt = resp.text.split('\n\n')[0].replace(/[ .]+$/,'').split('.')[0..1].join('. ') + '.'
        msgCtx.answerHTML "A Wikipédia diz que \"#{simpleHTML txt, removeHR: on}\"
                         \nSaiba mais sobre <a href=\"#{resp.url}\">#{resp.title}</a>.",
                         disable_web_page_preview: true
        #msgCtx.answer resp.image

exports.findByDDG = (msgCtx, match, q)->
  ddg.siteAPI q, ['pt-BR', 'pt', 'en'], (err, result)->
    if err?
      msgCtx.answer "Não sei #{match[1..].join ' '}. :-(".replace /\s+/g, ' '
      unless err?.message is 'no result.'
        msgCtx.bot.admDebug "DDG fail \"#{err}\" for query \"#{q}\"."
    else
      msgCtx.answerHTML "Achei isso no DuckDuckGo: \"#{result.content}\"
                       \nSaiba mais sobre <a href=\"#{result.url}\">#{result.title}</a>.",
                       disable_web_page_preview: true


