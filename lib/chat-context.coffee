{ log:logger, timeout, mkID } = require './util'

# Allows the bot to remember the talk context in a chat
module.exports = class ChatContext

  constructor: (@bot, @chatID)->
    @__memoFile = 'context-' + @chatID
    @ctx = @bot.memory.readJSON @__memoFile
    @ctx.lang ?= 'en'
    @__send = require('./telegram-api').send

  save: (callback)->
    @bot.memory.writeJSON @__memoFile, @ctx, callback

  # Prevent many automatic file writing from `set`s and `inc`s.
  delayedSave: ->
    return if @waitingToSave
    @waitingToSave = true
    timeout 1.5, =>
      @waitingToSave = false
      @save (err)-> logger.error err if err

  get: (key)->
    @ctx[key]

  set: (args...)->
    if 'object' is typeof args[0]
      obj = args[0]
    else
      obj = {}; i = 0
      obj[args[i++]] = args[i++] while args[i]?
    @ctx[key] = value for key, value of obj
    do @delayedSave
    @ctx[key]

  inc: (key)->
    @ctx[key] ?= 0
    @ctx[key]++
    do @delayedSave
    @ctx[key]

  computeCommand: (cmd, interactionModule)->
    if cmd?.lockContext
      lockContextID = do mkID
      @ctx.lockContext =
        id: lockContextID
        module: interactionModule
        timeout: cmd.lockContext.timeout or 10

      # Forgot this lockContext if it still valid:
      timeout @ctx.lockContext.timeout * 60, =>
        if @ctx.lockContext?.id is lockContextID
          @ctx.lockContext = null
    do @delayedSave if cmd?

  sendMD: (args...)-> @__send.md this, args...

  sendMsg: (args...)-> @__send.msg this, args...

  sendHTML: (args...)-> @__send.html this, args...

  sendLocation: (args...)-> @__send.location this, args...

