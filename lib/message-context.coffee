# Helps to send all needed information and functionality related to the
# message over different bot methods and modules.
module.exports = class MessageContext

  constructor: (message, @chatCtx)->
    toMeRE = ///
      ^\s*/?@?(#{@chatCtx.bot.username}|bot)\s*[,:\s]\s*|
      [,]?\s*@?(#{@chatCtx.bot.username}|bot)\s*$
      ///ig
    @bot = @chatCtx.bot
    @id = message.message_id
    @srcMessage = message
    @toMe = toMeRE.test message.text # TODO: test response messages
    @text = message.text.replace(toMeRE,'').replace /^\s*|\s*$/g, ''
    @from = message.from
    @__send = require('./telegram-api').send

  answer: (msg, opts={}, cb)->
    [cb, opts] = [opts, {}] if 'function' is typeof opts
    opts.reply_to_message_id = @id
    @__send.msg @chatCtx, msg, opts, cb

  answerMD: (msg, opts={}, cb)->
    [cb, opts] = [opts, {}] if 'function' is typeof opts
    opts.reply_to_message_id = @id
    @__send.md @chatCtx, msg, opts, cb

  answerHTML: (msg, opts={}, cb)->
    [cb, opts] = [opts, {}] if 'function' is typeof opts
    opts.reply_to_message_id = @id
    @__send.html @chatCtx, msg, opts, cb

  answerLocation: (lat, lon, opts={}, cb)->
    [cb, opts] = [opts, {}] if 'function' is typeof opts
    opts.reply_to_message_id = @id
    @__send.location @chatCtx, lat, lon, opts, cb

