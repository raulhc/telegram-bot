{parse:parseURL} = require 'url'
{mkID, log:logger} = require './util'
spawnSync = require('child_process').spawnSync

exports.protocolMod = protocolMod =
  http: require 'http'
  https: require 'https'
  chose: (httpOpts)-> protocolMod[httpOpts.protocol.replace /:$/, '']

exports.prepareHttpOpts = prepareHttpOpts = (url, headers={})->
  httpOpts = parseURL url if typeof(url) is 'string'
  httpOpts.headers ?= {}
  headers['User-Agent'] ?= 'Telegram Bot'
  headers['Accept-Language'] ?= 'en'
  headers['Accept-Encoding'] ?= 'identity'
  httpOpts.headers[k] = v for k,v of headers
  httpOpts

exports.mkTempAgent = mkTempAgent = (httpOpts)->
  protocol = protocolMod.chose httpOpts
  httpAgent = new protocol.Agent keepAlive: true
  httpAgent.name = httpOpts.host + ' ' + mkID()
  httpAgent.mustKillMe = true
  httpAgent.killMe = -> httpAgent.destroy() if httpAgent.mustKillMe
  httpAgent

# Make an http request, with promises, returning the data.
# request('http://gnu.org', 'Accept-Language':'pt-BR').then
#   (data, res)-> console.log "Page content: #{data}"
#   (err)-> console.error "Fail", err
exports.request = request = (url, headers={}, httpAgent)->
  httpOpts = prepareHttpOpts url, headers
  protocol = protocolMod.chose httpOpts
  httpAgent ?= mkTempAgent httpOpts
  httpOpts.agent = httpAgent
  logger.debug 'REQUEST', url:url, headers:headers, httpAgent:httpAgent.name or null
  new Promise (resolve, reject)->
    req = protocol.request httpOpts, (res)->
      encoding = res.headers['Content-Encoding'] or res.headers['content-encoding']
      data = if encoding is 'gzip' then new Buffer [] else ''
      res.on 'data', (chunk)->
        if data.constructor is Buffer
          data = Buffer.concat [data, chunk]
        else
          data += chunk
      res.on 'end', -> reqResp httpOpts, httpAgent, res, data, encoding, resolve, reject
    req.on 'error', reject
    do req.end
  .then (res)-> httpAgent.killMe?(); res
  .catch (err)-> httpAgent.killMe?(); throw err

# When response end, it use all generated data to build a response.
exports.reqResp = reqResp = (httpOpts, httpAgent, res, data, encoding, resolve, reject)->
  if 300 <= res.statusCode <= 399 and res.headers.location?
    newURL = computeRedirection httpOpts, res.headers.location
    request(newURL, httpOpts.headers, httpAgent)
      .then resolve
      .catch reject
  else if res.statusCode > 299
    err = Error res.statusMessage
    err.statusCode = res.statusCode
    reject err
  else
    if encoding is 'gzip'
      res.data = spawnSync('zcat',[],{input:data}).stdout.toString()
    else
      res.data = data
    resolve res

# Write a new full URL for 301 cases or page links
# `from` must be your current full URL.
# `to` can be any relative piece or a full URL.
exports.computeRedirection = computeRedirection = (from, to)->
  from = parseURL from if typeof(from) is 'string'
  fPort = if from.port then ':' + from.port else ''
  if to[0] is '?' # it is a new query
    return "#{from.protocol}//#{from.host}#{fPort}#{from.pathname}#{to}"
  if to.match '^https?://' # it is a full URL
    return to
  if to.match '^//' # it is a full URL without protocol
    return "#{from.protocol}#{to}"
  if to[0] is '/' # it is a new path from root
    return "#{from.protocol}//#{from.host}#{fPort}#{to}"
  # it is a relative path
  basePath = from.pathname.replace /[^\/]*$/, ''
  "#{from.protocol}//#{from.host}#{fPort}#{basePath}#{to}"

