spawnSync = require('child_process').spawnSync
{log:logger} = require './util'
fs = require 'fs'
os = require 'os'
telegramAPI = require './telegram-api'

# Envia um áudio sintetizado com espeak
module.exports = sendSpeak = (chatCtx, txt, opts={}, callback=(->))->
  if typeof opts is 'function'
    callback = opts
    opts = {}
  spawnSync = require('child_process').spawnSync
  txt = txt.replace /['"\\]/g, ''
  logger.debug 'Will generate voice with espeak...', text: txt
  fileName = "#{os.tmpdir()}/#{ Math.random().toString()[2..] }.mp3"
  out = spawnSync 'sh', ['-c', "espeak -v pt --stdout '#{txt}' | ffmpeg -i pipe:0 #{fileName}"]
  errTxt = out.stderr?.toString?() or "Status #{out.status}"
  if out.status isnt 0
    logger.error err = Error "espeak fail for \"#{txt}\": #{errTxt}"
    chatCtx.bot.admDebug err.message
    try fs.unlinkSync fileName; catch
    callback err
  else
    opts.file = fs.readFileSync fileName
    opts.fileName = 'voice.mp3'
    telegramAPI.api 'sendVoice', chatCtx, opts, (err, data)->
      if err?
        logger.error 'Voice ERR:', err, err.stack, 'return:', data
        chatCtx.bot.admDebug "sendSpeak Fail for \"#{txt}\"\n#{err}"
      try fs.unlinkSync fileName; catch
      callback err, data

