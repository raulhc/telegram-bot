{log:logger, timeout} = require './util'
{parse:parseURL} = require 'url'
https = require 'https'
ChatContext = require './chat-context'
MessageContext = require './message-context'

defaultAPICallback = (cmd, opts)->
  (err)->
    if err?
      admDebug "API Request #{cmd} Fail.\nErr: #{err}\nOpts: #{inspect opts}"

exports.buildApiCallOpts = buildApiCallOpts = (chatCtx, opts={})->
  if typeof opts is 'function'
    callback = opts
    opts = {}
  opts.chat_id = chatCtx.chatID
  opts

# Executa uma chamada na API do Telegram e entrega seu retorno assincronamente
exports.api = (cmd, chatCtx, opts={}, callback=defaultAPICallback(cmd, opts))->
  opts = buildApiCallOpts chatCtx, opts
  # Store opts to send in a error log if it fails:
  origOpts = {}
  origOpts[k]=v for k,v of opts
  # When uploading:
  field = cmd[4..].toLowerCase()
  fileData = opts.file
  fileName = opts.fileName or Math.random().toString(16).replace /0\./, ''
  delete opts.file
  delete opts.fileName
  # Build Request URL:
  apiBase = 'https://api.telegram.org/bot' + chatCtx.bot.token
  reqURL = "#{apiBase}/#{cmd}?" +
           (for k,v of opts
             v = JSON.stringify v if v?.constructor is Object
             "#{k}=#{encodeURIComponent v}"
           ).join '&'
  httpOpts = parseURL reqURL
  if fileData?
    querystring = require 'querystring'
    httpOpts.method = 'POST'
    httpOpts.headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
      #'Content-Length': Buffer.byteLength fileData
    }
  logger.debug 'API Req:', (httpOpts.method or 'GET'), httpOpts.href
  req = https.request httpOpts, (res)->
    if 500 <= res.statusCode <= 599
      timeout 10, -> exports.api cmd, origOpts, callback
      logger.error "API ERROR for #{reqURL}", statusCode:res.statusCode
    else
      data = ''
      res.on 'data', (chunk)-> data += chunk
      res.on 'end', ->
        try
          data = JSON.parse data
          if data.ok
            callback null, data
          else
            logger.error "API call fail for #{reqURL}", description:data.description
            callback new Error(data.description), data
        catch err
          logger.error "API Data fail for #{reqURL}", err:err, data:data
          chatCtx.bot.admDebug "API Data Fail: #{err}\n\nDATA: #{data}"
          callback err
  req.on 'error', (err)-> callback err
  if fileData?
    boundaryKey = Math.random().toString()[2..]
    req.setHeader 'Content-Type', "multipart/form-data; boundary=\"#{boundaryKey}\""
    req.write "--#{boundaryKey}\r\n" +
    #'Content-Type: application/octet-stream\r\n' +
    "Content-Disposition: form-data; name=\"#{field}\";
                                     filename=\"#{fileName}\"\r\n" +
    'Content-Transfer-Encoding: binary\r\n\r\n'
    req.write fileData
    req.write "\r\n--#{boundaryKey}--"
  do req.end

exports.send = send =
  # Envia mensagem textual em algum chat.
  msg: (chatCtx, msg, opts={}, callback)->
    if typeof opts is 'function'
      callback = opts
      opts = {}
    opts.text = msg
    exports.api 'sendMessage', chatCtx, opts, callback

  # Envia mensagem textual em algum chat formatado com Markdown.
  md: (chatCtx, msg, opts={}, callback)->
    if typeof opts is 'function'
      callback = opts
      opts = {}
    opts.parse_mode = 'Markdown'
    send.msg chatCtx, msg, opts, callback

  # Envia mensagem textual em algum chat formatado com HTML.
  html: (chatCtx, msg, opts={}, callback)->
    if typeof opts is 'function'
      callback = opts
      opts = {}
    opts.parse_mode = 'HTML'
    send.msg chatCtx, msg, opts, callback

  # Envia um localização geográfica e o cliente Telegram mostra no mapa.
  location: (chatCtx, lat, lon, opts={}, callback)->
    opts.latitude = lat
    opts.longitude = lon
    exports.api 'sendLocation', chatCtx, opts, callback

chatCtxCache = {}
exports.mkAdmChatCtx = mkAdmChatCtx = (bot)->
  return chatCtxCache[bot.username] if chatCtxCache[bot.username]?
  chatCtxCache[bot.username] = new ChatContext bot, bot.admChatID

exports.clearChatCtxCache = -> chatCtxCache = {}

# Envia uma notícia pra o admin
exports.mkAdmDebug = (bot)-> admDebug = (message, callback=(->))->
  logger.warn message
  if bot.admChatID?
    if message.constructor.name.match /Error/
      message = "#{message.message}\nAt #{
        message.stack.split('\n').join('#')
          .replace /.*?#[^#]* at [^#]*\(([^#]+\.(js|coffee):[:0-9]+)\).*/i, '$1'
        }"
    send.msg mkAdmChatCtx(bot), message, (err, response)->
      if err?
        text = "Can't send you a debug message. Please read the log"
        timeout 30, -> send.msg {bot:bot, chatID:bot.admChatID}, text, (->)
        logger.error 'API fail to send admDebug', debug: message, err: err
        callback Error "API fail to send admDebug \"#{message}\", because #{err.message}"
      else
        callback null, response
  else
    callback Error 'Cant contact admin to send debug: ' + message

# Pede atualizações ao Telegram via long polling.
exports.getUpdates = getUpdates = (bot, opts={}, callback)->
  opts.timeout ?= 40
  opts.delay ?= 4
  exports.api 'getUpdates', mkAdmChatCtx(bot), timeout: opts.timeout, offset: bot.lastUpdate+1, (err, data)->
    if data?.result?
      for update in data.result
        bot.setLastUpdate update.update_id
        if update?.message?.text?
          try
            bot.reactToText update.message
          catch err
            logger.error 'reactToText fail', err
            err = err.toString()[0..100] + '...' if err.toString().length > 100
            bot.admDebug "reactToText fail \n" + err
        else
          logger 'Update Desconhecido:', update
    if opts.live
      bot.__updateTimeout = timeout opts.delay, -> getUpdates bot, opts, callback
    callback? err, data

