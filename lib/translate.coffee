before = '(^|\\s|[,.;:!?])'
after  = '($|\\s|[,.;:!?])'
mePt   = 'eu|mim'
youPt  = 'vc|c[êe]|voc[êe]|t[úu]'

mePtRE  = ///#{mePt}///i
youPtRE = ///#{youPt}///i

translateMe2You = [
  [ /sou/i          , 'é'      ]
  [ /serei/i        , 'será'   ]
  [ /fui/i          , 'foi'    ]
  [ /estou|t[ôo]u?/i, 'está'   ]
  [ /(es)?tarei/i   , 'estará' ]
  [ /(es)?teve/i    , 'esteve' ]
  [ /tenho/i        , 'tem'    ]
  [ /terei/i        , 'terá'   ]
  [ /tive/i         , 'teve'   ]
  [ /vou/i          , 'vai'    ]
  [ /irei/i         , 'irá'    ]
  [ /fui/i          , 'foi'    ]
  [ /([a-z]r)/i     ,  null    ]
]

translateYou2Me = [
  [ /é|eh/i         , 'sou'    ]
  [ /ser[áa]/i      , 'serei'  ]
  [ /foi/i          , 'fui'    ]
  [ /t[áa]|est[áa]/i, 'estou'  ]
  [ /estar[áa]/i    , 'estarei']
  [ /esteve/i       , 'estive' ]
  [ /tem/i          , 'tenho'  ]
  [ /ter[áa]/i      , 'terei'  ]
  [ /teve/i         , 'tive'   ]
  [ /vai/i          , 'vou'    ]
  [ /ir[áa]/i       , 'irei'   ]
  [ /foi/i          , 'fui'    ]
  [ /([a-z]r)/i     ,  null    ]
]

badWords = [
  [ 's[ei]\\s*fud[êe]r?'             , 'fornicar consigo mesmo' ]
  [ 'fud[êe]r?'                      , 'fornicar'       ]
  [ 'fud[êe]u'                       , 'fornicou'       ]
  [ 'c[úu](?:z[ãa]o|z[õo]es|zona)?s?', 'ânus'           ]
  [ '(?:pi[ck]a|piro[ck]a|caralho)s?', 'pênis'          ]
  [ 'buceta|(x[óo])+ta'              , 'vulva'          ]
  [ 'bucetas|(x[óo])+tas'            , 'vulvas'         ]
  [ 'uma\\s+(?:bosta|merda)'         , 'um cocôzinho'   ]
  [ 'umas?\\s+(?:bosta|merda)s'      , 'uns cocôzinhos' ]
  [ 'bosta|merda'                    , 'cocôzinho'      ]
  [ 'bostas|merdas'                  , 'cocôzinhos'     ]
  [ 'est[úu]pido'                    , 'desagradável'   ]
  [ 'est[úu]pidos'                   , 'desagradáveis'  ]
  [ '(?:um\\s*)?(?:imbecil|idiota|retardado|burro)', 'pouco inteligente' ]
  [ '(?:uns\\s*)?(?:imbecil?|idiota|retardado|burro)s', 'pouco inteligentes' ]
]

pronomMatch = ///#{before}(#{mePt}|#{youPt})\s+([^\s]+)#{after}///ig
ownMatch    = ///#{before}(meu|[st]eu)#{after}///ig

# Translate "me" to "you" and "you" to "me".
exports.pronoms = (text)->
  text
    .replace pronomMatch, (match, g...)->
      if youPtRE.test g[1]
        for trans in translateYou2Me
          transW = trans[1] or g[2]
          if trans[0].test g[2] then return "#{g[0]}eu #{transW}#{g[3]}"
        match # if no You2Me translation, return the original match
      else
        for trans in translateMe2You
          transW = trans[1] or g[2]
          if trans[0].test g[2] then return "#{g[0]}você #{transW}#{g[3]}"
        match # if no Me2You translation, return the original match
    .replace ownMatch, (match, g...)->
      if g[1].toLowerCase() is 'meu'
        "#{g[0]}seu#{g[2]}"
      else
        "#{g[0]}meu#{g[2]}"

# Translate bad words
exports.badWords = (text)->
  for word in badWords
    text = text.replace ///#{before}(#{word[0]})#{after}///ig, "$1#{word[1]}$3"
  text

