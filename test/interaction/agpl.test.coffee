should = require 'should'
{showMeTheCodeRE, listen} = require '../../interaction/agpl'
{ silenceOutputs, restoreOutputs, mkMsgCtx,
  lastTelegramApiCall, rmLastTelegramApiCall } = require '../test-helper'

Bot = require '../../telegram-bot'

describe 'interaction/agpl', ->

  before silenceOutputs
  after restoreOutputs
  afterEach rmLastTelegramApiCall
  @timeout 8000

  it 'match show me the code expressions in a message', ->
    'show me the code'.should.match showMeTheCodeRE
    'show me your code!?'.should.match showMeTheCodeRE
    'show me your source.'.should.match showMeTheCodeRE

  it 'answer show me the code message', (done)->
    bot = new Bot \
      name:'My Bot', username:'mybot',
      token:'ABCD', admChatID:'1234',
      configDir: false, srcRepo: 'http://example.com/myrepo.git'
    bot.loadDefaultInteractions ->
      bot.reactToText \
        message_id: 9876, text:'show me the code',
        chat:{id:'c123'}, from:{id:'u123', username:'someone'}
      lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
      lastTelegramApiCall().opts.chat_id.should.be.equal 'c123'
      lastTelegramApiCall().opts.reply_to_message_id.should.be.equal 9876
      lastTelegramApiCall().opts.text.should.be.equal 'Aqui someone: http://example.com/myrepo.git'
      do done

  it 'answer show me the code message with undefined repo', (done)->
    bot = new Bot \
      name:'My Bot', username:'mybot',
      token:'ABCD', admChatID:'1234',
      configDir: false
    bot.loadDefaultInteractions ->
      bot.reactToText \
        message_id: 9977, text:'show me the code',
        chat:{id:'c456'}, from:{id:'u456', username:'someone'}
      lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
      lastTelegramApiCall().opts.chat_id.should.be.equal 'c456'
      lastTelegramApiCall().opts.reply_to_message_id.should.be.equal 9977
      lastTelegramApiCall().opts.text.should.be.equal 'Aqui someone: No... Hiding.'
      do done

