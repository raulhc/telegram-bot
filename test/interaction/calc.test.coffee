should = require 'should'
{mathRE, matchDate, evalMath, translateToJS, listen} = require '../../interaction/calc'
{ silenceOutputs, restoreOutputs,
  lastTelegramApiCall, rmLastTelegramApiCall,
 mkMsgCtx, shouldThrow} = require '../test-helper'

describe 'interaction/calc', ->

  before silenceOutputs
  after restoreOutputs
  afterEach rmLastTelegramApiCall

  it 'match aritimetic expressions in a message', ->
    ('2+3?'.match mathRE)[2].should.be.equal '2+3'
    ('2 + 3?'.match mathRE)[2].should.be.equal '2 + 3'
    ('2 + (3)?'.match mathRE)[2].should.be.equal '2 + (3)'
    ('I have 2 + 3 bananas.'.match mathRE)[2].should.be.equal '2 + 3'
    ('1 have 2 + 3 bananas.'.match mathRE)[2].should.be.equal '2 + 3'
    ('I have 2 + 3 bananas 1.'.match mathRE)[2].should.be.equal '2 + 3'
    ('I have 2 + 3 bananas 1 + 1.'.match mathRE)[2].should.be.equal '2 + 3'
    ('robot2 -1 + 5.'.match mathRE)[2].should.be.equal '-1 + 5'

  testCases = [
    ['2 + 3'          , '2+3'                   ,  5]
    ['-2 + 3 + (4)'   , '-2+3+(4)'              ,  5]
    ['-2 + 3 + (-4)'  , '-2+3+(-4)'             , -3]
    ['-2 + 3 + (4+5)' , '-2+3+(4+5)'            , 10]
    ['2 +- 3'         , '2+-3'                  , -1]
    ['2 +-- 3'        , '2+--3'                 ,  Error 'Invalid left-hand side expression in prefix operation']
    ['2 ++ 3'         , '2++3'                  ,  Error 'Invalid left-hand side expression in postfix operation']
    ['(2 + 3))'       , '(2+3))'                ,  Error 'Unexpected token )']
    ['2^3'            , 'Math.pow(2,3)'         ,  8]
    ['-2^-3'          , '-Math.pow(2,-3)'       , -0.125]
    ['2)^3'           ,  Error 'Bad structure before power']
    ['2^(3'           ,  Error 'Bad structure after power']
    ['(2)^(3)'        , 'Math.pow((2),(3))'     ,  8]
    ['-(2)^-(3)'      , '-Math.pow((2),-(3))'   , -0.125]
    ['2³'             , 'Math.pow(2,3)'         ,  8]
    ['2¹⁸'            , 'Math.pow(2,18)'        , 262144]
    ['2⁵+2¹²'         , 'Math.pow(2,5)+Math.pow(2,12)', 4128]
    ['(2)³'           , 'Math.pow((2),3)'       ,  8]
    ['√9'             , 'Math.pow(9,1/2)'       ,  3]
    ['∛8'             , 'Math.pow(8,1/3)'       ,  2]
    ['∜16'            , 'Math.pow(16,1/4)'      ,  2]
    ['3+∜16'          , '3+Math.pow(16,1/4)'    ,  5]
    ['√(10+15)'       , 'Math.pow((10+15),1/2)' ,  5]
    ['√((10)'         ,  Error 'Bad root parenthesis structure']
    ['√9+3'           , 'Math.pow(9,1/2)+3'     ,  6]
    ['-√(6+3)'        , '-Math.pow((6+3),1/2)'  , -3]
    ['-√+(6+3)'       , '-Math.pow(+(6+3),1/2)' , -3]
    ['√.04'           , 'Math.pow(.04,1/2)'     ,  0.2]
    ['√-9'            , 'Math.pow(-9,1/2)'      ,  Error 'Invalid expression']
    ['√(-9)'          , 'Math.pow((-9),1/2)'    ,  Error 'Invalid expression']
    ['1/0'            , '1/0'                   ,  Infinity]
    ['-1/0'           , '-1/0'                  , -Infinity]
  ]

  it 'translate aritimetic expression to JS', ->
    for t in testCases
      if t[1].constructor is Error
        shouldThrow t[0], t[1].message, -> translateToJS t[0]
      else
        should.doesNotThrow (-> translateToJS t[0]), Error, "Translation of #{t[0]}"
        translateToJS(t[0]).should.be.equal t[1], "Translation of #{t[0]}"

  it 'evaluate aritimetic expression', ->
    for t in testCases
      if (t[2] or t[1]).constructor is Error
        shouldThrow t[0], (t[2] or t[1]).message, -> evalMath t[0]
      else
        should.doesNotThrow (-> evalMath t[0]), Error, "Evaluation of #{t[0]}"
        evalMath(t[0]).should.be.equal t[2], "Evaluation of #{t[0]}"

  it 'answer to a aritimetic embeded message', ->
    msgCtx = mkMsgCtx text: 'I have 2 + 3 bananas.'
    listen msgCtx
    lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
    lastTelegramApiCall().opts.text.should.be.equal '<code>2+3</code> = <code>5</code>'
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    msgCtx.text.should.be.equal 'I have 5 bananas.'

  it 'complain about bad aritimetic embeded message', ->
    listen mkMsgCtx text: 'I have 2+(3 bananas.'
    lastTelegramApiCall().opts.text.should.match \
      /<code>2\+\(3<\/code> = <code>.*<\/code>\nUnexpected end of input/

