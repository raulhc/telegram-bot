should = require 'should'
{calendarThisMonthRE, calendarSomeMonthRE, calendarSomeMonthRE, calendarSomeYearRE,
 askForHour, askForDay, listen} = require '../../interaction/date-time'
{ silenceOutputs, restoreOutputs, mkMsgCtx,
  lastTelegramApiCall, rmLastTelegramApiCall } = require '../test-helper'

describe 'interaction/date-time', ->

  before silenceOutputs
  after restoreOutputs

  it 'recognize hour request', ->
    'que hora são'.should.match askForHour
    'que horas são'.should.match askForHour
    'que horas sao'.should.match askForHour
    'que horas são?'.should.match askForHour
    'que horas são ? !?'.should.match askForHour
    'que horas são agora?'.should.match askForHour
    'anything que horas são agora?'.should.match askForHour
    'que horas são agora? anything'.should.not.match askForHour

  it 'recognize date request', ->
    'que dia é hoje? anything'.should.not.match askForDay
    m = 'que dia é hoje'.match askForDay
    m[1].should.be.equal 'hoje'
    m = 'que data eh hoje'.match askForDay
    m[1].should.be.equal 'hoje'
    m = 'que dia é hoje?'.match askForDay
    m[1].should.be.equal 'hoje'
    m = 'anything que dia é hoje?'.match askForDay
    m[1].should.be.equal 'hoje'
    m = 'que dia é amanhã?'.match askForDay
    m[1].should.be.equal 'amanhã'
    m = 'que dia será amanha?'.match askForDay
    m[1].should.be.equal 'amanha'
    m = 'que dia foi ontem?'.match askForDay
    m[1].should.be.equal 'ontem'

  it 'recognize calendar request for this month', ->
    'calendário'.should.match calendarThisMonthRE
    'calendário?'.should.match calendarThisMonthRE
    'anything calendário'.should.not.match calendarThisMonthRE
    'calendário anything'.should.not.match calendarThisMonthRE
    'mostre calendário'.should.match calendarThisMonthRE
    'anything mostre o calendário'.should.match calendarThisMonthRE
    'mostre o calendário anything'.should.match calendarThisMonthRE
    'anything mostre o calendário anything'.should.match calendarThisMonthRE
    'mostre o calendário'.should.match calendarThisMonthRE
    'mostre o calendario'.should.match calendarThisMonthRE
    'mostrar o calendario'.should.match calendarThisMonthRE
    'escreva o calendário'.should.match calendarThisMonthRE
    'escrever o calendário'.should.match calendarThisMonthRE
    'apresente o calendário'.should.match calendarThisMonthRE
    'apresentar o calendário'.should.match calendarThisMonthRE
    'faça o calendário'.should.match calendarThisMonthRE
    'faca o calendário'.should.match calendarThisMonthRE
    'fazer o calendário'.should.match calendarThisMonthRE
    'desenhe o calendário'.should.match calendarThisMonthRE
    'desenhar o calendário'.should.match calendarThisMonthRE
    'mostre o calendário desse mês.'.should.match calendarThisMonthRE
    'mostre o calendário deste mês.'.should.match calendarThisMonthRE
    'mostre o calendário para este mês.'.should.match calendarThisMonthRE

  it 'recognize calendar request for some month', ->
    'calendário'.should.not.match calendarSomeMonthRE
    'calendário desse ano'.should.not.match calendarSomeMonthRE
    'mostre o calendário desse ano'.should.not.match calendarSomeMonthRE
    'anything calendário do mês 2'.should.not.match calendarSomeMonthRE
    'mostre o calendário desse mês'.should.not.match calendarSomeMonthRE
    m = 'mostre calendário do próximo mês'.match calendarSomeMonthRE
    m[1].should.be.equal 'próximo mês'
    should(m[2]).be.not.ok()
    m = 'mostre o calendário do mês que vem'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês que vem'
    should(m[2]).be.not.ok()
    m = 'mostre calendário do mês passado'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês passado'
    should(m[2]).be.not.ok()
    m = 'mostre calendário do mês 2'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês 2'
    should(m[2]).be.not.ok()
    m = 'mostre calendário do mês 2!'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês 2'
    should(m[2]).be.not.ok()
    m = 'mostre calendário do mês 2!?'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês 2'
    should(m[2]).be.not.ok()
    m = 'mostre calendário do mês 2 !'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês 2'
    should(m[2]).be.not.ok()
    m = 'mostre o calendário do mes 2'.match calendarSomeMonthRE
    m[1].should.be.equal 'mes 2'
    should(m[2]).be.not.ok()
    m = 'anything mostre o calendário do mês 2'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês 2'
    should(m[2]).be.not.ok()
    m = 'mostre o calendário do mês 2 anything'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês 2'
    should(m[2]).be.not.ok()
    m = 'calendário do mês 2'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês 2'
    should(m[2]).be.not.ok()
    m = 'calendário do mês 2.'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês 2'
    should(m[2]).be.not.ok()
    m = 'anything mostre o calendário do mês 2 anything'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês 2'
    should(m[2]).be.not.ok()
    m = 'mostre o calendário de fevereiro'.match calendarSomeMonthRE
    m[1].should.be.equal 'fevereiro'
    should(m[2]).be.not.ok()
    m = 'mostre o calendário do mês fevereiro'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês fevereiro'
    should(m[2]).be.not.ok()
    m = 'mostre o calendario do mes de fevereiro'.match calendarSomeMonthRE
    m[1].should.be.equal 'mes de fevereiro'
    should(m[2]).be.not.ok()
    m = 'mostre o calendário do mês fevereiro de 2016'.match calendarSomeMonthRE
    m[1].should.be.equal 'mês fevereiro'
    m[2].should.be.equal '2016'
    m = 'mostre o calendário de fevereiro de 2016'.match calendarSomeMonthRE
    m[1].should.be.equal 'fevereiro'
    m[2].should.be.equal '2016'
    m = 'anything mostre calendário de fevereiro de 2016 anything'.match calendarSomeMonthRE
    m[1].should.be.equal 'fevereiro'
    m[2].should.be.equal '2016'

  it 'recognize calendar request for some year', ->
    'calendário'.should.not.match calendarSomeYearRE
    'anything calendário do ano 2016'.should.not.match calendarSomeYearRE
    'calendário desse mês'.should.not.match calendarSomeYearRE
    m = 'calendário desse ano'.match calendarSomeYearRE
    m[1].should.be.equal 'desse ano'
    m = 'calendário deste ano.'.match calendarSomeYearRE
    m[1].should.be.equal 'deste ano'
    m = 'calendário deste ano anything.'.match calendarSomeYearRE
    m[1].should.be.equal 'deste ano'
    m = 'o calendário deste ano.'.match calendarSomeYearRE
    m[1].should.be.equal 'deste ano'
    m = 'anything mostre calendário deste ano.'.match calendarSomeYearRE
    m[1].should.be.equal 'deste ano'
    m = 'anything mostre o calendário deste ano.'.match calendarSomeYearRE
    m[1].should.be.equal 'deste ano'
    m = 'calendário do ano que vem'.match calendarSomeYearRE
    m[1].should.be.equal 'ano que vem'
    m = 'calendário do próximo ano'.match calendarSomeYearRE
    m[1].should.be.equal 'próximo ano'
    m = 'calendário do ano passado'.match calendarSomeYearRE
    m[1].should.be.equal 'ano passado'
    m = 'calendário de 79'.match calendarSomeYearRE
    m[1].should.be.equal '79'
    m = 'calendário ano 79'.match calendarSomeYearRE
    m[1].should.be.equal 'ano 79'
    m = 'calendário ano 79.'.match calendarSomeYearRE
    m[1].should.be.equal 'ano 79'
    m = 'calendário ano 79 anything'.match calendarSomeYearRE
    m[1].should.be.equal 'ano 79'
    m = 'calendário do ano de 79'.match calendarSomeYearRE
    m[1].should.be.equal 'ano de 79'
    m = 'calendario do ano 2016'.match calendarSomeYearRE
    m[1].should.be.equal 'ano 2016'
    m = 'anything mostre o calendário do ano 2016'.match calendarSomeYearRE
    m[1].should.be.equal 'ano 2016'
    m = 'mostre o calendário do ano 2016 anything'.match calendarSomeYearRE
    m[1].should.be.equal 'ano 2016'

  it 'answer hour request', (done)->
    listen mkMsgCtx(text: 'que horas são?'), ->
      lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
      lastTelegramApiCall().opts.chat_id.should.be.equal 123
      lastTelegramApiCall().opts.text.should.match /^Pra mim são .* \([0-9]{2}:[0-9]{2}\)\.$/i
      do done

  today = new Date
  w = 'dom seg ter qua qui sex sáb'.split ' '
  m = 'jan fev mar abr mai jun jul ago set out nov dez'.split ' '

  it 'answer date request for today', (done)->
    listen mkMsgCtx(text: 'que dia é hoje?'), ->
      lastTelegramApiCall().opts.text.should.match ///^Hoje\sé\s
        #{w[today.getDay()]}[a-z]+,\s#{today.getDate()}\sde\s#{m[today.getMonth()]}[a-z]+\.
        $///i
      lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
      lastTelegramApiCall().opts.chat_id.should.be.equal 123
      do done

  it 'answer date request for yesterday', (done)->
    (yesterday = new Date).setDate today.getDate() - 1
    listen mkMsgCtx(text: 'que dia foi ontem?'), ->
      lastTelegramApiCall().opts.text.should.match ///^Ontem\sfoi\s
        #{w[yesterday.getDay()]}[a-z]+,\s#{yesterday.getDate()}\sde\s#{m[yesterday.getMonth()]}[a-z]+\.
        $///i
      do done

  it 'answer date request for tomorrow', (done)->
    (tomorrow = new Date).setDate today.getDate() + 1
    listen mkMsgCtx(text: 'que dia será amanhã?'), ->
      lastTelegramApiCall().opts.text.should.match ///^Amanhã\sserá\s
        #{w[tomorrow.getDay()]}[a-z]+,\s#{tomorrow.getDate()}\sde\s#{m[tomorrow.getMonth()]}[a-z]+\.
        $///i
      do done

  describe 'answer calendar requests', ->

    d = '\\s[0-9x]{2}\\s'
    dayLines = "(\\n#{d}\\|#{d}\\|#{d}\\|#{d}\\|#{d}\\|#{d}\\|\\s[0-9x]{2}\\s?)+"

    it 'for this month', (done)->
      listen mkMsgCtx(text: 'calendário'), ->
        lastTelegramApiCall().opts.text.should.match ///^
          <b>\s#{m[today.getMonth()]}[a-z]+\sde\s#{today.getFullYear()}<\/b>#{dayLines}$///i
        lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
        lastTelegramApiCall().opts.chat_id.should.be.equal 123

        listen mkMsgCtx(text: 'calendário desse mês'), ->
          lastTelegramApiCall().opts.text.should.match ///^
            <b>\s#{m[today.getMonth()]}[a-z]+\sde\s#{today.getFullYear()}<\/b>#{dayLines}$///i
          do done

    it 'for last month', (done)->
      listen mkMsgCtx(text: 'calendário do mês passado'), ->
        (date = new Date).setMonth date.getMonth() - 1
        lastTelegramApiCall().opts.text.should.match ///^
          <b>\s#{m[date.getMonth()]}[a-z]+\sde\s#{date.getFullYear()}<\/b>#{dayLines}$///i
        lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
        lastTelegramApiCall().opts.chat_id.should.be.equal 123
        do done

    it 'for next month', (done)->
      listen mkMsgCtx(text: 'calendário do mês que vem'), ->
        (date = new Date).setMonth date.getMonth() + 1
        lastTelegramApiCall().opts.text.should.match ///^
          <b>\s#{m[date.getMonth()]}[a-z]+\sde\s#{date.getFullYear()}<\/b>#{dayLines}$///i
        lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
        lastTelegramApiCall().opts.chat_id.should.be.equal 123
        do done

    it 'for some month', (done)->
      listen mkMsgCtx(text: 'calendário de janeiro de 2016'), ->
        date = new Date 2016, 0, 1
        lastTelegramApiCall().opts.text.should.be.equal '<b> Janeiro de 2016</b>\n
           xx | xx | xx | xx | xx | 01 | 02 \n
           03 | 04 | 05 | 06 | 07 | 08 | 09 \n
           10 | 11 | 12 | 13 | 14 | 15 | 16 \n
           17 | 18 | 19 | 20 | 21 | 22 | 23 \n
           24 | 25 | 26 | 27 | 28 | 29 | 30 \n
           31 | xx | xx | xx | xx | xx | xx'
        lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
        lastTelegramApiCall().opts.chat_id.should.be.equal 123

        listen mkMsgCtx(text: 'calendário do mês de janeiro de 2016'), ->
          date = new Date 2016, 0, 1
          lastTelegramApiCall().opts.text.should.be.equal '<b> Janeiro de 2016</b>\n
             xx | xx | xx | xx | xx | 01 | 02 \n
             03 | 04 | 05 | 06 | 07 | 08 | 09 \n
             10 | 11 | 12 | 13 | 14 | 15 | 16 \n
             17 | 18 | 19 | 20 | 21 | 22 | 23 \n
             24 | 25 | 26 | 27 | 28 | 29 | 30 \n
             31 | xx | xx | xx | xx | xx | xx'
          lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
          lastTelegramApiCall().opts.chat_id.should.be.equal 123
          do done

    it 'for this year', (done)->
      listen mkMsgCtx(text: 'calendário desse ano'), ->
        year = (new Date).getFullYear()
        lastTelegramApiCall().opts.text.should.match ///^
          <b>\sJaneiro\sde\s#{year}<\/b>#{dayLines}\n\s\n
          <b>\sFevereiro\sde\s#{year}<\/b>#{dayLines}\n\s\n
          (<b>\s(#{m.join '|'})[a-zç]+\sde\s#{year}<\/b>#{dayLines}\n\s\n){9}
          <b>\sDezembro\sde\s#{year}<\/b>#{dayLines}
          $///i
        lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
        lastTelegramApiCall().opts.chat_id.should.be.equal 123
        do done

    it 'for last year', (done)->
      listen mkMsgCtx(text: 'calendário do ano passado'), ->
        year = (new Date).getFullYear() - 1
        lastTelegramApiCall().opts.text.should.match ///^
          <b>\sJaneiro\sde\s#{year}<\/b>#{dayLines}\n\s\n
          <b>\sFevereiro\sde\s#{year}<\/b>#{dayLines}\n\s\n
          (<b>\s(#{m.join '|'})[a-zç]+\sde\s#{year}<\/b>#{dayLines}\n\s\n){9}
          <b>\sDezembro\sde\s#{year}<\/b>#{dayLines}
          $///i
        lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
        lastTelegramApiCall().opts.chat_id.should.be.equal 123
        do done

    it 'for next year', (done)->
      listen mkMsgCtx(text: 'calendário do ano que vem'), ->
        year = (new Date).getFullYear() + 1
        lastTelegramApiCall().opts.text.should.match ///^
          <b>\sJaneiro\sde\s#{year}<\/b>#{dayLines}\n\s\n
          <b>\sFevereiro\sde\s#{year}<\/b>#{dayLines}\n\s\n
          (<b>\s(#{m.join '|'})[a-zç]+\sde\s#{year}<\/b>#{dayLines}\n\s\n){9}
          <b>\sDezembro\sde\s#{year}<\/b>#{dayLines}
          $///i
        lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
        lastTelegramApiCall().opts.chat_id.should.be.equal 123
        do done

    it 'for some year', (done)->
      listen mkMsgCtx(text: 'calendário de 2016'), ->
        year = 2016
        lastTelegramApiCall().opts.text.should.be.equal '<b> Janeiro de 2016</b>\n
            xx | xx | xx | xx | xx | 01 | 02 \n
            03 | 04 | 05 | 06 | 07 | 08 | 09 \n
            10 | 11 | 12 | 13 | 14 | 15 | 16 \n
            17 | 18 | 19 | 20 | 21 | 22 | 23 \n
            24 | 25 | 26 | 27 | 28 | 29 | 30 \n
            31 | xx | xx | xx | xx | xx | xx\n
            \n<b> Fevereiro de 2016</b>\n
            xx | 01 | 02 | 03 | 04 | 05 | 06 \n
            07 | 08 | 09 | 10 | 11 | 12 | 13 \n
            14 | 15 | 16 | 17 | 18 | 19 | 20 \n
            21 | 22 | 23 | 24 | 25 | 26 | 27 \n
            28 | 29 | xx | xx | xx | xx | xx\n
            \n<b> Março de 2016</b>\n
            xx | xx | 01 | 02 | 03 | 04 | 05 \n
            06 | 07 | 08 | 09 | 10 | 11 | 12 \n
            13 | 14 | 15 | 16 | 17 | 18 | 19 \n
            20 | 21 | 22 | 23 | 24 | 25 | 26 \n
            27 | 28 | 29 | 30 | 31 | xx | xx\n
            \n<b> Abril de 2016</b>\n
            xx | xx | xx | xx | xx | 01 | 02 \n
            03 | 04 | 05 | 06 | 07 | 08 | 09 \n
            10 | 11 | 12 | 13 | 14 | 15 | 16 \n
            17 | 18 | 19 | 20 | 21 | 22 | 23 \n
            24 | 25 | 26 | 27 | 28 | 29 | 30\n
            \n<b> Maio de 2016</b>\n
            01 | 02 | 03 | 04 | 05 | 06 | 07 \n
            08 | 09 | 10 | 11 | 12 | 13 | 14 \n
            15 | 16 | 17 | 18 | 19 | 20 | 21 \n
            22 | 23 | 24 | 25 | 26 | 27 | 28 \n
            29 | 30 | 31 | xx | xx | xx | xx\n
            \n<b> Junho de 2016</b>\n
            xx | xx | xx | 01 | 02 | 03 | 04 \n
            05 | 06 | 07 | 08 | 09 | 10 | 11 \n
            12 | 13 | 14 | 15 | 16 | 17 | 18 \n
            19 | 20 | 21 | 22 | 23 | 24 | 25 \n
            26 | 27 | 28 | 29 | 30 | xx | xx\n
            \n<b> Julho de 2016</b>\n
            xx | xx | xx | xx | xx | 01 | 02 \n
            03 | 04 | 05 | 06 | 07 | 08 | 09 \n
            10 | 11 | 12 | 13 | 14 | 15 | 16 \n
            17 | 18 | 19 | 20 | 21 | 22 | 23 \n
            24 | 25 | 26 | 27 | 28 | 29 | 30 \n
            31 | xx | xx | xx | xx | xx | xx\n
            \n<b> Agosto de 2016</b>\n
            xx | 01 | 02 | 03 | 04 | 05 | 06 \n
            07 | 08 | 09 | 10 | 11 | 12 | 13 \n
            14 | 15 | 16 | 17 | 18 | 19 | 20 \n
            21 | 22 | 23 | 24 | 25 | 26 | 27 \n
            28 | 29 | 30 | 31 | xx | xx | xx\n
            \n<b> Setembro de 2016</b>\n
            xx | xx | xx | xx | 01 | 02 | 03 \n
            04 | 05 | 06 | 07 | 08 | 09 | 10 \n
            11 | 12 | 13 | 14 | 15 | 16 | 17 \n
            18 | 19 | 20 | 21 | 22 | 23 | 24 \n
            25 | 26 | 27 | 28 | 29 | 30 | xx\n
            \n<b> Outubro de 2016</b>\n
            xx | xx | xx | xx | xx | xx | 01 \n
            02 | 03 | 04 | 05 | 06 | 07 | 08 \n
            09 | 10 | 11 | 12 | 13 | 14 | 15 \n
            16 | 17 | 18 | 19 | 20 | 21 | 22 \n
            23 | 24 | 25 | 26 | 27 | 28 | 29 \n
            30 | 31 | xx | xx | xx | xx | xx\n
            \n<b> Novembro de 2016</b>\n
            xx | xx | 01 | 02 | 03 | 04 | 05 \n
            06 | 07 | 08 | 09 | 10 | 11 | 12 \n
            13 | 14 | 15 | 16 | 17 | 18 | 19 \n
            20 | 21 | 22 | 23 | 24 | 25 | 26 \n
            27 | 28 | 29 | 30 | xx | xx | xx\n
            \n<b> Dezembro de 2016</b>\n
            xx | xx | xx | xx | 01 | 02 | 03 \n
            04 | 05 | 06 | 07 | 08 | 09 | 10 \n
            11 | 12 | 13 | 14 | 15 | 16 | 17 \n
            18 | 19 | 20 | 21 | 22 | 23 | 24 \n
            25 | 26 | 27 | 28 | 29 | 30 | 31'
        lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
        lastTelegramApiCall().opts.chat_id.should.be.equal 123
        do done

