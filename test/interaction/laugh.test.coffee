should = require 'should'
{laughRE, laughBelowTimeout, listen} = require '../../interaction/laugh'
{ silenceOutputs, restoreOutputs, mkChatCtx, mkMsgCtx,
  lastTelegramApiCall, rmLastTelegramApiCall } = require '../test-helper'

describe 'interaction/laugh', ->

  before silenceOutputs
  after restoreOutputs
  afterEach rmLastTelegramApiCall

  it 'match laughing expressions in a message', ->
    'hi hehehe!'.should.match laughRE
    'hi heeehe!'.should.match laughRE
    'hi he!'.should.not.match laughRE
    'It did that! 😄'.should.match laughRE

  it 'send laughing message on first laugh listen', ->
    msgCtx = mkMsgCtx text: 'hi hehehe!'
    do rmLastTelegramApiCall
    listen msgCtx
    lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.text.should.match /^((he)+|(hi)+|😄😄😅|Rá)[.!]*$/
    msgCtx.chatCtx.get('laughCounter').should.be.equal 1
    msgCtx.chatCtx.get('laughTimeout').should.be.greaterThan Date.now() + 60*1000

  it 'send no laughing message on second laugh listen', ->
    msgCtx = mkMsgCtx text: 'hi hehehe!'
    do rmLastTelegramApiCall
    msgCtx.chatCtx.set 'laughCounter', 1
    listen msgCtx
    lastTelegramApiCall().should.be.deepEqual {}
    msgCtx.chatCtx.get('laughCounter').should.be.equal 2

  it 'send a bad laughing message on third laugh listen', ->
    msgCtx = mkMsgCtx text: 'hi hehehe!'
    do rmLastTelegramApiCall
    msgCtx.chatCtx.set 'laughCounter', 2
    listen msgCtx
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.text.should.match /^(Tá tá\.\.\.|😄😊|nem teve tanta graça)$/
    msgCtx.chatCtx.get('laughCounter').should.be.equal 3

  it 'send no laughing message on fourth laugh listen', ->
    msgCtx = mkMsgCtx text: 'hi hehehe!'
    do rmLastTelegramApiCall
    msgCtx.chatCtx.set 'laughCounter', 3
    listen msgCtx
    #lastTelegramApiCall().should.be.deepEqual {}
    msgCtx.chatCtx.get('laughCounter').should.be.equal 4

  it 'send a bad laughing message on fifth laugh listen', ->
    msgCtx = mkMsgCtx text: 'hi hehehe!'
    do rmLastTelegramApiCall
    msgCtx.chatCtx.set 'laughCounter', 4
    listen msgCtx
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.text.should.match /^(como riem!|já deu\.\.\.|nem teve tanta graça)$/
    msgCtx.chatCtx.get('laughCounter').should.be.equal 5

  it 'reset laughCounter and answer laught when laughTimeout', ->
    msgCtx = mkMsgCtx text: 'hi hehehe!'
    msgCtx.chatCtx.set 'laughCounter', 5
    msgCtx.chatCtx.set 'laughTimeout', Date.now() - 1000
    do rmLastTelegramApiCall
    listen msgCtx
    lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.text.should.match /^((he)+|(hi)+|😄😄😅|Rá)[.!]*$/
    msgCtx.chatCtx.get('laughCounter').should.be.equal 1
    msgCtx.chatCtx.get('laughTimeout').should.be.greaterThan Date.now() + 60*1000

  it 'recognize if it is below the laugh timeout', ->
    chatCtx = mkChatCtx 123
    chatCtx.set 'laughTimeout', Date.now() + 999
    laughBelowTimeout(chatCtx).should.be.equal true
    chatCtx.set 'laughTimeout', Date.now() - 5
    laughBelowTimeout(chatCtx).should.be.equal false

