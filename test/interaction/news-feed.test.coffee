should = require 'should'
{feedRelatedRE, feedAddRE, feedRemoveRE,
 feedListRE, feedShowRE, feedHelpRE, feedUndefRE,
 NewsFeed, listen} = require '../../interaction/news-feed'
{ silenceOutputs, restoreOutputs, mkMsgCtx, mkBot,
  lastTelegramApiCall, rmLastTelegramApiCall } = require '../test-helper'

describe 'interaction/news-feed', ->

  before silenceOutputs
  after restoreOutputs
  news = null
  beforeEach ->
    news = new NewsFeed mkBot()
  afterEach rmLastTelegramApiCall

  it 'match feed related message', ->
    'feed'.should.match feedRelatedRE
    'rss'.should.match feedRelatedRE
    'FEED'.should.match feedRelatedRE
    'RSS'.should.match feedRelatedRE
    'feed something'.should.not.match feedRelatedRE
    'what is feed?'.should.not.match feedRelatedRE
    'plz feed help'.should.match feedRelatedRE
    'feed add'.should.not.match feedRelatedRE
    'feed add http://example.com'.should.match feedRelatedRE
    'add the rss http://example.com'.should.match feedRelatedRE
    'feed ls'.should.match feedRelatedRE
    'feed list'.should.match feedRelatedRE
    'feed show 2 from FOSS'.should.match feedRelatedRE

  it 'recognize an acceptable request', ->
    testAcceptable = (txt, toMe)->
      news.acceptableRequest(mkMsgCtx text:txt, toMe:toMe, bot:news.bot).should.be.true
    testUnacceptable = (txt, toMe)->
      news.acceptableRequest(mkMsgCtx text:txt, toMe:toMe, bot:news.bot).should.be.false
    testAcceptable 'feed'
    testAcceptable 'feed add http://example.com'
    testAcceptable 'Adicione o feed http://example.com'
    testAcceptable 'feed help something', true
    testUnacceptable 'feed help something'
    testAcceptable 'something feed ls', true
    testUnacceptable 'something feed ls'

  it 'match feed add in a message', ->
    ''.should.match feedAddRE

  it 'match feed remove in a message', ->
    ''.should.match feedRemoveRE

  it 'match feed list in a message', ->
    ''.should.match feedListRE

  it 'match feed show in a message', ->
    ''.should.match feedShowRE

  it 'match feed help in a message', ->
    ''.should.match feedHelpRE

  it 'match feed undef command in a message', ->
    ''.should.match feedUndefRE

