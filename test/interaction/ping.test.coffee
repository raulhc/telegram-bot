should = require 'should'
{listen} = require '../../interaction/ping'
{ silenceOutputs, restoreOutputs, mkMsgCtx,
  lastTelegramApiCall, rmLastTelegramApiCall } = require '../test-helper'

describe 'interaction/ping', ->

  before silenceOutputs
  after restoreOutputs

  it 'pong back', ->
    msgCtx = mkMsgCtx text: 'ping'
    do rmLastTelegramApiCall
    listen msgCtx
    lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.text.should.be.equal 'pong'

