should = require 'should'
{queryRE, findByDDG, listen} = require '../../interaction/what-is'
{ silenceOutputs, restoreOutputs,
  lastTelegramApiCall, rmLastTelegramApiCall,
  mkChatCtx, mkMsgCtx} = require '../test-helper'
{normalize} = require 'path'
wikipedia = require '../../lib/wikipedia'
ddg = require '../../lib/ddg'

describe 'interaction/what-is', ->

  before silenceOutputs
  after restoreOutputs
  afterEach rmLastTelegramApiCall

  it 'match "what is..." expressions in a message', ->
    match = 'O que é limonada'.match queryRE
    match[5].should.be.equal 'limonada'
    should(match[6]).be.not.ok()
    match = 'O que é limonada?'.match queryRE
    match[5].should.be.equal 'limonada'
    match[6].should.be.equal '?'
    match = 'O que é uma limonada?'.match queryRE
    match[5].should.be.equal 'limonada'
    match[6].should.be.equal '?'
    match = 'O que é uma grande limonada gelada?'.match queryRE
    match[5].should.be.equal 'grande limonada gelada'
    match[6].should.be.equal '?'
    match = 'O que é uma limonada gelada? Hum?'.match queryRE
    match[5].should.be.equal 'limonada gelada'
    match[6].should.be.equal '?'

  it 'response to a "what is..." message', ->
    wikipedia.query = (query, langs, cb)-> cb null, {
        title: 'Limonade'
        text: 'Limonade is a nice drink.'
        url: 'http://wikipedia.org/limonade'
      }
    msgCtx = mkMsgCtx text: 'O que é uma limonada?'
    listen msgCtx
    lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.disable_web_page_preview.should.be.equal true
    lastTelegramApiCall().opts.parse_mode.should.be.equal 'HTML'
    lastTelegramApiCall().opts.text.should.be.equal 'A Wikipédia diz que
      "Limonade is a nice drink." \nSaiba mais sobre
      <a href="http://wikipedia.org/limonade">Limonade</a>.'

  it 'find answer by DDG', ->
    ddg.siteAPI = (q, langs, cb)-> cb null, {
        title: 'Limonade Site'
        content: 'Limonade is a nice drink.'
        url: 'http://some.where/limonade'
      }
    msgCtx = mkMsgCtx text:''
    match = [ null, 'o', 'que', 'é', 'uma', 'limonada' ]
    q = 'limonada'
    findByDDG msgCtx, match, q
    lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.disable_web_page_preview.should.be.equal true
    lastTelegramApiCall().opts.parse_mode.should.be.equal 'HTML'
    lastTelegramApiCall().opts.text.should.be.equal 'Achei isso no DuckDuckGo:
      "Limonade is a nice drink." \nSaiba mais sobre
      <a href="http://some.where/limonade">Limonade Site</a>.'

  it 'cant find answer by DDG', ->
    ddg.siteAPI = (q, langs, cb)-> cb Error 'cant find'
    msgCtx = mkMsgCtx text:''
    msgCtx.chatCtx.bot.admDebug = (->)
    match = [ null, 'o', 'que', 'é', 'uma', 'limonada' ]
    q = 'limonada'
    findByDDG msgCtx, match, q
    lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.text.should.be.equal 'Não sei o que é uma limonada. :-('

