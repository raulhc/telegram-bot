should = require 'should'
whereIs = require '../../interaction/where-is'
{ silenceOutputs, restoreOutputs,
  lastTelegramApiCall, rmLastTelegramApiCall,
  mkChatCtx, mkMsgCtx} = require '../test-helper'
{normalize} = require 'path'
wikipedia = require '../../lib/wikipedia'
ddg = require '../../lib/ddg'

describe 'interaction/where-is', ->

  before silenceOutputs
  after restoreOutputs
  afterEach rmLastTelegramApiCall

  it 'match "where is..." expressions in a message', ->
    match = 'Onde fica Brasilia?'.match whereIs.whereRE
    match[3].should.be.equal 'Brasilia'

  it 'response to a "where is...", with a place', ->
    whereIs.findPlace = (query, cb)-> cb null, { lat:-15.793, lng:-47.882 }
    msgCtx = mkMsgCtx message_id: 1111, text: 'Onde fica Brasilia?'
    whereIs.listen msgCtx
    lastTelegramApiCall().cmd.should.be.equal 'sendLocation'
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.reply_to_message_id.should.be.equal 1111
    lastTelegramApiCall().opts.latitude.should.be.equal -15.793
    lastTelegramApiCall().opts.longitude.should.be.equal -47.882

  it 'response to a "where is...", when cant find the place', ->
    err = Error 'cant find address'
    err.status = 'ZERO_RESULTS'
    whereIs.findPlace = (query, cb)-> cb err
    msgCtx = mkMsgCtx message_id: 1111, text: 'Onde fica Brasilia?'
    whereIs.listen msgCtx
    lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.reply_to_message_id.should.be.equal 1111
    lastTelegramApiCall().opts.text.should.be.equal 'Não consegui achar Brasilia.
      :-(\nIsso é mesmo um endereço?'

