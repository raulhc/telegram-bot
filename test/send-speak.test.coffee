should = require 'should'
sendSpeak = require '../lib/send-speak'
{ silenceOutputs, restoreOutputs, mkChatCtx,
  lastTelegramApiCall, rmLastTelegramApiCall } = require './test-helper'

describe 'lib/send-speak', ->

  before silenceOutputs
  after restoreOutputs
  chatCtx = null
  beforeEach -> chatCtx = mkChatCtx 123
  afterEach rmLastTelegramApiCall

  it 'send audio message', (done)->
    sendSpeak chatCtx, 'hi', (err, data)->
      should(err).be.null()
      lastTelegramApiCall().cmd.should.be.equal 'sendVoice'
      lastTelegramApiCall().opts.chat_id.should.be.equal 123
      file = lastTelegramApiCall().opts.file.toString()
      header = 'ID3\u0004\u0000\u0000\u0000\u0000\u0000#TSSE\u0000\u0000\u0000\u000f\u0000\u0000\u0003Lavf'
      file[0..24].should.be.equal header
      lastTelegramApiCall().opts.file.length.should.be.equal 2317
      do done

