should = require 'should'
MessageContext = require '../lib/message-context'
ChatContext = require '../lib/chat-context'
Bot = require '../telegram-bot'
api = require '../lib/telegram-api'

{silenceOutputs, restoreOutputs, lastLog,
 lastTelegramApiCall, rmLastTelegramApiCall,
 mkTemp, rmDir, timeout } = require './test-helper'

describe 'lib/telegram-api', ->

  before silenceOutputs
  after ->
    do rmLastTelegramApiCall
    do restoreOutputs

  bot = null
  beforeEach ->
    do api.clearChatCtxCache
    bot = new Bot \
      name:'My Bot', username:'mybot',
      token:'ABCD', admChatID:'9876',
      configDir: false

  afterEach -> api.mockApi = null

  describe 'admDebug', ->

    it 'send message to the admin and log it', (done)->
      admDebug = api.mkAdmDebug bot
      admDebug 'test message', (err, resp)->
        should(err).be.null()
        lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
        lastTelegramApiCall().opts.chat_id.should.be.equal '9876'
        lastTelegramApiCall().opts.text.should.be.equal 'test message'
        lastLog().$msg.should.be.deepEqual 'test message'
        do done

    it 'log when there is no admChatID', (done)->
      bot.admChatID = null
      do rmLastTelegramApiCall
      admDebug = api.mkAdmDebug bot
      admDebug 'test message', (err, resp)->
        err.message.should.be.equal 'Cant contact admin to send debug: test message'
        lastLog().$msg.should.be.deepEqual 'test message'
        lastTelegramApiCall().should.be.deepEqual {}
        do done

    it 'send error to the admin', (done)->
      admDebug = api.mkAdmDebug bot
      admDebug Error('Some Fail'), (err, resp)->
        should(err).be.null()
        lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
        lastTelegramApiCall().opts.chat_id.should.be.equal '9876'
        lastTelegramApiCall().opts.text.should.match /^Some Fail\nAt \/.*\/telegram-api\.test\.coffee:[:0-9]+$/
        do done

  describe 'send a...', ->

    chatCtx = null
    beforeEach ->
      chatCtx = new ChatContext bot, 123

    it 'plain text message', (done)->
      api.send.msg chatCtx, 'Hi Nobody!',{}, ->
        lastTelegramApiCall().should.be.deepEqual
          cmd: "sendMessage"
          opts:
            chat_id: 123
            text: "Hi Nobody!"
        do done

    it 'markdown message', (done)->
      api.send.md chatCtx, 'Hi *Nobody*!',{}, ->
        lastTelegramApiCall().should.be.deepEqual
          cmd: "sendMessage"
          opts:
            chat_id: 123
            parse_mode: "Markdown"
            text: "Hi *Nobody*!"
        do done

    it 'html message', (done)->
      api.send.html chatCtx, 'Hi <b>Nobody</b>!',{}, ->
        lastTelegramApiCall().should.be.deepEqual
          cmd: "sendMessage"
          opts:
            chat_id: 123
            parse_mode: "HTML"
            text: "Hi <b>Nobody</b>!"
        do done

    it 'location', (done)->
      api.send.location chatCtx, -12.9722, -38.5014, {}, ->
        lastTelegramApiCall().should.be.deepEqual
          cmd: "sendLocation"
          opts:
            chat_id: 123
            latitude: -12.9722
            longitude: -38.5014
        do done

  it 'get updates, one time, without data', (done)->
    api.getUpdates bot, live:false, ->
      lastTelegramApiCall().cmd.should.be.equal 'getUpdates'
      lastTelegramApiCall().opts.offset.should.be.equal 1
      lastTelegramApiCall().opts.timeout.should.be.equal 40
      do done

