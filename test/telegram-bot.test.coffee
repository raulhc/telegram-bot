should = require 'should'
spawn = require('child_process').spawn
fs = require 'fs'

Bot = require '../telegram-bot'

{ silenceOutputs, restoreOutputs,
  lastTelegramApiCall, rmLastTelegramApiCall,
  keys, mkTemp, rmDir } = require './test-helper'

describe 'telegram-bot', ->

  temp = null
  beforeEach -> temp = do mkTemp
  afterEach -> rmDir temp

  describe 'act as a stand alone script, then', ->

    opts = {}
    beforeEach ->
      fs.mkdirSync temp + '/.config'
      opts = env: {PATH: process.env.PATH, HOME: temp}, cwd: __dirname + '/..'

    it 'help the user when bot_token is not provided', (done)->
      bot = spawn './telegram-bot.coffee', [], opts, ->
      errOut = ''
      bot.stderr.on 'data', (data)-> errOut += data
      bot.on 'close', (code)->
        errOut.should.match /export bot_token=/
        code.should.be.equal 1
        do done

    it 'run when recives all env vars', (done)->
      opts.env.bot_token = '1234567'
      opts.env.bot_username = 'testBot'
      opts.env.bot_adm_chat_id = '7654321'
      bot = spawn './telegram-bot.coffee', [], opts, ->
      txtOut = ''
      errOut = ''
      bot.stdout.on 'data', (data)->
        txtOut += data
        bot.kill 'SIGHUP' if 0 <= txtOut.indexOf 'I%20am%20ALIVE'
      bot.stderr.on 'data', (data)->
        errOut += data
        bot.kill 'SIGHUP' if 0 <= errOut.indexOf '\n'
      bot.on 'close', (code)->
        txtOut.should.match /I%20am%20ALIVE[^"]*chat_id=7654321/
        errOut.should.be.equal ''
        do done

  describe 'whith a bot object', ->

    before silenceOutputs
    after restoreOutputs

    bot = null
    beforeEach ->
      bot = new Bot \
        name:'My Bot', username:'mybot',
        token:'ABCD', admChatID:'1234',
        configDir: temp

    describe 'react to text', ->

      it 'listen a simple text', ->
        bot.addInteraction listen: (msgCtx)->
          msgCtx.chatCtx.sendMsg "Listened #{msgCtx.text} from #{msgCtx.from.id}"
        bot.reactToText text:'some text', chat:{id:'chat123'}, from:{id:'usr123'}
        lastTelegramApiCall().opts.chat_id.should.be.equal 'chat123'
        lastTelegramApiCall().opts.text.should.be.equal 'Listened some text from usr123'

    describe 'get chat context', ->

      it 'return a ChatContext instance and cache it', ->
        chatCtx = bot.getChatContext 123
        chatCtx.constructor.name.should.be.equal 'ChatContext'
        chatCtx.testMe = 'ok!'
        chatCtx2 = bot.getChatContext 123
        chatCtx2.testMe.should.be.equal 'ok!'

    describe 'get updates', ->

      api = require '../lib/telegram-api'
      afterEach -> api.mockApi = null

      it 'one time, with data', (done)->
        api.mockApi = (cmd, opts)-> result: [{
          update_id: 111
          message:
            chat: id: 123
            text: 'Hi!'
            from: username: 'someone'
        }]
        bot.addInteraction listen: (msgCtx)-> msgCtx.chatCtx.sendMsg 'Hello!'
        bot.getUpdates live:false, ->
          lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
          lastTelegramApiCall().opts.chat_id.should.be.equal 123
          lastTelegramApiCall().opts.text.should.be.equal 'Hello!'
          do done

      it 'live mode', (done)->
        num = 0
        api.mockApi = (cmd, opts)-> result: [{
          update_id: 111 + num
          message:
            chat: id: 123
            text: 'say ' + num
            from: username: 'someone'
        }]
        bot.addInteraction listen: (msgCtx)->
          num++
          if /^say [012]$/.test msgCtx.text
            msgCtx.chatCtx.sendMsg 'ok'
          else
            done Error 'Unespected test message.'
        bot.getUpdates live:true, timeout:9, delay:0.2, ->
          lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
          lastTelegramApiCall().opts.chat_id.should.be.equal 123
          lastTelegramApiCall().opts.text.should.be.equal 'ok'
          if num is 3
            do bot.stopUpdate
            do done

      it 'run interation initializer for this bot', (done)->
        bot.addInteraction
          listen: (msgCtx)-> null
          init: (bot)-> do done

    describe 'default interactions', ->

      it 'load and answer with that', (done)->
        bot.loadDefaultInteractions ->
          do rmLastTelegramApiCall
          bot.reactToText text:'hi', chat:{id:'c123'}, from:{id:'u123', username:'someone'}
          lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
          lastTelegramApiCall().opts.chat_id.should.be.equal 'c123'
          lastTelegramApiCall().opts.text.length.should.be.greaterThan 2
          bot.reactToText text:'que horas são?', chat:{id:'c456'}, from:{id:'u456', username:'someone'}
          lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
          lastTelegramApiCall().opts.chat_id.should.be.equal 'c456'
          lastTelegramApiCall().opts.text.should.match /^Pra mim são .*\.$/
          do done

