os = require 'os'
fs = require 'fs'
util = require '../lib/util'
assert = require 'assert'
MessageContext = require '../lib/message-context'
ChatContext = require '../lib/chat-context'
Bot = require '../telegram-bot'

# Array from spaced string of words
exports.A = (str)-> str.split ' '

exports.timeout = (secs, callback)-> setTimeout callback, secs*1000

exports.keys = (obj)-> Object.keys(obj).sort()

exports.mkTemp = mkTemp = -> fs.mkdtempSync os.tmpdir() + '/bot-test'

exports.rmDir = rmDir = (dir)->
  for f in fs.readdirSync dir
    try
      fs.unlinkSync dir + '/' + f
    catch
      rmDir dir + '/' + f
  fs.rmdirSync dir

originalLogWriter = util.logWriter

exports.silenceOutputs = ->
  do exports.silenceLogWriter
  do exports.silenceTelegramApi
exports.restoreOutputs = ->
  do exports.restoreLogWriter
  do exports.restoreTelegramApi

lastLog = {}
exports.silenceLogWriter = ->
  util.logWriter = (obj)-> lastLog = obj

exports.restoreLogWriter = ->
  util.logWriter = originalLogWriter

exports.lastLog = -> lastLog

exports.rmLastLog = -> lastLog = {}

telegramAPI = require '../lib/telegram-api'
originalTelegramApiMethod = telegramAPI.api

lastTelegramApiCall = {}
exports.lastTelegramApiCall = -> lastTelegramApiCall

exports.rmLastTelegramApiCall = -> lastTelegramApiCall = {}

exports.silenceTelegramApi = ->
  telegramAPI.api = (cmd, msgCtx, opts={}, callback=(->))->
    opts = telegramAPI.buildApiCallOpts msgCtx, opts
    lastTelegramApiCall = cmd: cmd, opts: opts
    if @mockApi?
      callback null, @mockApi cmd, opts
    else
      callback null, ok: true

exports.restoreTelegramApi = ->
  telegramAPI.api = originalTelegramApiMethod

exports.mkBot = (config={})->
  config.name      = 'My Bot' unless config.name?
  config.username  = 'mybot'  unless config.username?
  config.token     = 'ABCD'   unless config.token?
  config.admChatID = '9876'   unless config.admChatID?
  config.configDir = false    unless config.configDir?
  new Bot config

exports.mkChatCtx = (chatID, bot=exports.mkBot())->
  new ChatContext bot, chatID

exports.mkMsgCtx = (opts={})->
  chatCtx = exports.mkChatCtx 123, opts.bot
  opts.from ?= {}
  opts.from.username ?= 'nobody'
  new MessageContext opts, chatCtx

# Enable and Simplify throw test with comment.
exports.shouldThrow = (it, errorMessage, fn)->
  try do fn; catch err
  assert.ok err, "#{it} must throw an error."
  err.message.should.be.equal errorMessage, "#{it} must throw this error message."

