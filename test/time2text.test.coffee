should = require 'should'
Time2Text = require '../lib/time2text'

describe 'lib/time2text', ->

  it 'get week day name', ->
    tt = new Time2Text 'Pt-BR'
    tt.getDayName(new Date 2016,0,1).should.be.equal 'sexta'
    tt.getDayName(new Date 2016,6,31).should.be.equal 'domingo'
    tt.getDayName(new Date(2016,0,1), false).should.be.equal 'sex'
    tt.getDayName(new Date(2016,6,31), false).should.be.equal 'dom'

  it 'get date', ->
    tt = new Time2Text 'Pt-BR'
    tt.getDate(new Date 2016,0,1).should.be.equal 'sexta, 1 de janeiro'
    tt.getDate(new Date 2016,6,31).should.be.equal 'domingo, 31 de julho'
    tt.getDate(new Date(2016,0,1), false).should.be.equal '1 de janeiro'
    tt.getDate(new Date(2016,0,1), false, true).should.be.equal '1 de janeiro de 2016'
    tt.getDate(new Date(2016,0,1), true, true).should.be.equal 'sexta, 1 de janeiro de 2016'

  it 'get humanized time', ->
    tt = new Time2Text 'Pt-BR'
    tt.getTime(new Date 0,0,0, 23,40).should.be.equal 'quase meia noite'
    tt.getTime(new Date 0,0,0, 23,59).should.be.equal 'quase meia noite'
    tt.getTime(new Date 0,0,0,  0, 0).should.be.equal 'meia noite'
    tt.getTime(new Date 0,0,0,  0,10).should.be.equal 'meia noite'
    tt.getTime(new Date 0,0,0,  0,20).should.be.equal 'meia noite e meia'
    tt.getTime(new Date 0,0,0,  0,40).should.be.equal 'quase uma da madrugada'
    tt.getTime(new Date 0,0,0,  1, 0).should.be.equal 'uma da madrugada'
    tt.getTime(new Date 0,0,0,  1,19).should.be.equal 'uma da madrugada'
    tt.getTime(new Date 0,0,0,  1,20).should.be.equal 'uma e meia da madrugada'
    tt.getTime(new Date 0,0,0,  1,30).should.be.equal 'uma e meia da madrugada'
    tt.getTime(new Date 0,0,0,  1,39).should.be.equal 'uma e meia da madrugada'
    tt.getTime(new Date 0,0,0,  1,40).should.be.equal 'quase duas da madrugada'
    tt.getTime(new Date 0,0,0,  1,50).should.be.equal 'quase duas da madrugada'
    tt.getTime(new Date 0,0,0,  2, 0).should.be.equal 'duas da madrugada'
    tt.getTime(new Date 0,0,0,  3, 0).should.be.equal 'três da madrugada'
    tt.getTime(new Date 0,0,0,  4, 0).should.be.equal 'quatro da madrugada'
    tt.getTime(new Date 0,0,0,  5, 0).should.be.equal 'cinco da madrugada'
    tt.getTime(new Date 0,0,0,  6, 0).should.be.equal 'seis da madrugada'
    tt.getTime(new Date 0,0,0,  6,30).should.be.equal 'seis e meia da madrugada'
    tt.getTime(new Date 0,0,0,  6,50).should.be.equal 'quase sete da manhã'
    tt.getTime(new Date 0,0,0,  7, 0).should.be.equal 'sete da manhã'
    tt.getTime(new Date 0,0,0, 10, 0).should.be.equal 'dez da manhã'
    tt.getTime(new Date 0,0,0, 11,30).should.be.equal 'onze e meia da manhã'
    tt.getTime(new Date 0,0,0, 11,50).should.be.equal 'quase meio dia'
    tt.getTime(new Date 0,0,0, 12, 0).should.be.equal 'meio dia'
    tt.getTime(new Date 0,0,0, 12,30).should.be.equal 'meio dia e meia'
    tt.getTime(new Date 0,0,0, 12,40).should.be.equal 'quase uma da tarde'
    tt.getTime(new Date 0,0,0, 13, 0).should.be.equal 'uma da tarde'
    tt.getTime(new Date 0,0,0, 18,30).should.be.equal 'seis e meia da tarde'
    tt.getTime(new Date 0,0,0, 19, 0).should.be.equal 'sete da noite'
    tt.getTime(new Date 0,0,0, 23, 0).should.be.equal 'onze da noite'
    tt.getTime(new Date 0,0,0, 23,35).should.be.equal 'onze e meia da noite'

  it 'write a calendar', ->
    tt = new Time2Text 'Pt-BR'
    cal = tt.mkCalendar 1, 2016 # Feb
    cal.should.be.equal '<b> Fevereiro de 2016</b>\n
     xx | 01 | 02 | 03 | 04 | 05 | 06 \n
     07 | 08 | 09 | 10 | 11 | 12 | 13 \n
     14 | 15 | 16 | 17 | 18 | 19 | 20 \n
     21 | 22 | 23 | 24 | 25 | 26 | 27 \n
     28 | 29 | xx | xx | xx | xx | xx'

  it 'write a calendar, starting at sunday', ->
    tt = new Time2Text 'Pt-BR'
    cal = tt.mkCalendar 4, 2016 # May
    cal.should.be.equal '<b> Maio de 2016</b>\n
     01 | 02 | 03 | 04 | 05 | 06 | 07 \n
     08 | 09 | 10 | 11 | 12 | 13 | 14 \n
     15 | 16 | 17 | 18 | 19 | 20 | 21 \n
     22 | 23 | 24 | 25 | 26 | 27 | 28 \n
     29 | 30 | 31 | xx | xx | xx | xx'

    cal = tt.mkCalendar 0, 2017 # Jan
    cal.should.be.equal '<b> Janeiro de 2017</b>\n
     01 | 02 | 03 | 04 | 05 | 06 | 07 \n
     08 | 09 | 10 | 11 | 12 | 13 | 14 \n
     15 | 16 | 17 | 18 | 19 | 20 | 21 \n
     22 | 23 | 24 | 25 | 26 | 27 | 28 \n
     29 | 30 | 31 | xx | xx | xx | xx'

  it 'write a calendar with marked days', ->
    tt = new Time2Text 'Pt-BR'
    cal = tt.mkCalendar 6, 2015, 15:'http://day.abc/15', 20:'http://day.abc/20'
    cal.should.be.equal '<b> Julho de 2015</b>\n
     xx | xx | xx | 01 | 02 | 03 | 04 \n
     05 | 06 | 07 | 08 | 09 | 10 | 11 \n
     12 | 13 | 14 | <a href="http://day.abc/15">15</a> | 16 | 17 | 18 \n
     19 | <a href="http://day.abc/20">20</a> | 21 | 22 | 23 | 24 | 25 \n
     26 | 27 | 28 | 29 | 30 | 31 | xx'

