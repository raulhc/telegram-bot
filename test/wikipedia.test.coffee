should = require 'should'

wikipedia = require '../lib/wikipedia'

{keys, A, silenceOutputs, restoreOutputs} = require './test-helper'

describe 'lib/wikipedia', ->
  @timeout 6e3

  before silenceOutputs
  after restoreOutputs

  it 'finds a query', (done)->
    wikipedia.query 'Raul Seixas', ['pt', 'en'], (err, resp)->
      should(err).be.null()
      keys(resp).should.be.deepEqual A 'image lang query text title url'
      should(resp.err).be.not.ok
      resp.image.should.match /https?:\/\/upload\.wikimedia\.org\/.*\.jpg/
      resp.query.should.be.equal 'Raul Seixas'
      resp.lang.should.be.equal 'pt'
      resp.title.should.be.equal 'Raul Seixas'
      resp.text.should.match /<b>Raul Santos Seixas<\/b>/
      resp.url.should.be.equal 'https://pt.wikipedia.org/wiki/Raul_Seixas'
      do done

  it 'notify when cant find the the query', (done)->
    wikipedia.query 'Inexistentword', ['pt'], (err, resp)->
      err.message.should.be.equal 'no result.'
      err.query.should.be.equal 'Inexistentword'
      err.lang.should.be.equal 'pt'
      should(resp).be.not.ok()
      do done

  it 'finds a query in a second lang', (done)->
    wikipedia.query 'intergovernmental organization', ['pt', 'en'], (err, resp)->
      should(err).be.null()
      keys(resp).should.be.deepEqual A 'image lang query text title url'
      should(resp.err).be.not.ok()
      resp.title.should.be.equal 'Intergovernmental organization'
      resp.url.should.be.equal 'https://en.wikipedia.org/wiki/Intergovernmental_organization'
      do done

